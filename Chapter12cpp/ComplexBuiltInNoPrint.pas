PROGRAM ComplexBuiltInNoPrint;

TYPE
    mystring = ARRAY[1..3] OF char;

VAR
    x, y, z : complex;

{    
PROCEDURE print(expr : mystring; VAR z : complex);
    BEGIN
        write(expr, ' = (', z.re:0:5, ', ', z.im:0:5, ') ');
    END;
    }

BEGIN {ComplexTest}
    x.re := 3; x.im := 2;  {print('  x', x);} 
    write('  x', ' = (', x.re, ', ', x.im, ') '); 
    y.re := 8; y.im := -5; {print('  y', y);} 
    write('  y', ' = (', y.re, ', ', y.im, ') ');
    z := x + y;            {print('x+y', z);}    
    {write('x-y', ' = (', z.re:0:5, ', ', z.im:0:5, ') ');}
    writeln; 

    {print('  x', x);} 
    {write('  x', ' = (', x.re, ', ', x.im, ') ');}
    {print('  y', y);} 
    {write('  y', ' = (', y.re, ', ', y.im, ') ');}
    z := x - y; {print('x-y', z);} 
    {write('x-y', ' = (', z.re, ', ', z.im, ') ');}
    writeln;
    
    x.re := 4; x.im := -2; {print('  x', x);} 
    write('  x', ' = (', x.re, ', ', x.im, ') ');
    y.re := 1; y.im := -5; {print('  y', y);} 
    write('  y', ' = (', y.re, ', ', y.im, ') ');
    z := x*y;              {print('x*y', z);} 
    {write('x*y', ' = (', z.re, ', ', z.im, ') ');}
    writeln;

    x.re := -3; x.im := 2;  {print('  x', x);} 
    {write('  x', ' = (', x.re, ', ', x.im, ') ');}
    y.re := 3;  y.im := -6; {print('  y', y);} 
    {write('  y', ' = (', y.re, ', ', y.im, ') ');}
    z := x/y;               {print('x/y', z);} 
    {write('x/y', ' = (', z.re, ', ', z.im, ') ');}
    writeln;
    
    x.re := 5; x.im := 0; {print('  x', x);} 
    write('  x', ' = (', x.re, ', ', x.im, ') ');
    y.re := 3; y.im := 2; {print('  y', y);} 
    write('  y', ' = (', y.re, ', ', y.im, ') ');
    z := x + y;           {print('x+y', z);} 
    {write('x+y', ' = (', z.re, ', ', z.im, ') ');}
    writeln;
    
    x.re := 5; x.im := 4; {print('  x', x);} 
    write('  x', ' = (', x.re, ', ', x.im, ') ');
    y.re := 2; y.im := 0; {print('  y', y);} 
    write('  y', ' = (', y.re, ', ', y.im, ') ');
    z := x*y;             {print('x*y', z);} 
    {write('x*y', ' = (', z.re, ', ', z.im, ') ');}
    writeln;
    
    x.re := -2; x.im := -4; {print('  x', x);} 
    write('  x', ' = (', x.re, ', ', x.im, ') ');
    y.re := 0;  y.im :=  1; {print('  y', y);} 
    write('  y', ' = (', y.re, ', ', y.im, ') '); 
    writeln;
    z := x/y;               {print('x/y', z);} 
    {write('x/y', ' = (', z.re, ', ', z.im, ') ');}
    
    writeln;
END {ComplexTest}.
